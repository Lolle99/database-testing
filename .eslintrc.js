module.exports = {
  env: {
      browser: true,
      es2021: true
  },
  extends: [
      "plugin:react/recommended",
      "standard"
  ],
  parserOptions: {
      ecmaFeatures: {
          jsx: true
      },
      ecmaVersion: 12,
      sourceType: "module"
  },
  plugins: [
      "react"
  ],
  rules: {
      indent: ["error", 4],
      quotes: ["error", "double"],
      "require-jsdoc": ["error", {
        "require": {
            "FunctionDeclaration": true,
            "MethodDefinition": false,
            "ClassDeclaration": false,
            "ArrowFunctionExpression": false,
            "FunctionExpression": false
        }
    }]
  }
}
